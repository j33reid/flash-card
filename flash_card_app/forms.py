from django.forms import ModelForm
from flash_card_app.models import Flash_card


class Flash_card_form(ModelForm):
    class Meta:
        model = Flash_card
        fields = [
            "name",
            "front",
            "back",
        ]
